## Тестовый проект 

### Исполнитель

Ющенко Дмитрий Владимирович

### Стэк технологий

|   HTML	        |   CSS 	    |   JS  	    |
|:-----------------:|:-------------:|:---------------------:|
|  <a href="https://angular.io" target="_blanc"><img src="https://www.freelancejob.ru/upload/398/229ac1e72a328c663889bb2d14023f12.png" width="50" alt="Anhular Logo" /></a> 	|   <a href="https://nestjs.com" target="_blanc"><img src="https://luxe-host.ru/wp-content/uploads/c/7/e/c7e17df8eff5cebec0267448871e345d.png" width="50" alt="Nest Logo" /></a>	|   <a href="https://postgresql.org" target="_blanc"><img src="https://assets-global.website-files.com/62038ffc9cd2db4558e3c7b7/6242e5dd4337267623f1e7a5_js.svg" width="50" alt="Postgre Logo" /></a>	|



### Ссылки 

[Развёрнутый на сайт](https://dimcom1010.ru/ "dimcom1010.ru")

[Ссылка на GitLab](https://gitlab.com/work8922942/test-3 "gitlab.com")

### Контакты

 (МТС) +375 33 642-06-58
 telegram @Dimcom1010
