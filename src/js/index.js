import { spriteInit } from "./sprite.js";

const [burgerMenu, blockingLayer] = [".burger-menu", ".blocking-layer"].map(
    (e) => document.querySelector(e)
);

burgerMenu.addEventListener("click", function () {
    [burgerMenu, blockingLayer].forEach((e, i) => {
        if (e.classList.contains("active")) {
            e.classList.remove("active");
            !i && clearingMenuItems();
        } else {
            e.classList.add("active");
            !i && transferMenuItems();
        }
    });
});

// блок переноса пунктов меню при активации меню бургер
const [navUl, navMobileUl, actionButtons] = [
    ".nav-ul",
    ".nav-mobile-ul",
    ".action-buttons",
].map((e) => document.querySelector(e));

const transferMenuItems = () => {
    let menuItems = Array.from(navUl.children);
    const menuButtons = Array.from(actionButtons.children);
    menuItems = [...menuItems, ...menuButtons];
    let currentIndex = 0;

    const intervalId = setInterval(() => {
        if (currentIndex < menuItems.length) {
            navMobileUl.appendChild(menuItems[currentIndex].cloneNode(true));
            currentIndex++;
        } else {
            clearInterval(intervalId);
        }
    }, 50);
};

const clearingMenuItems = () => {
    navMobileUl.innerHTML = "";
};

spriteInit();
const languageTypes = {
    ru: "ru",
    eng: "en",
};

let languageState = languageTypes.ru;

const language = document.querySelector(".language");
const lang = document.querySelector(".lang");

lang.innerText = languageState.toUpperCase();

language.addEventListener("click", function (event) {
    event.stopPropagation();
    console.log("languageState", world);
    languageState =
        languageState === languageTypes.ru
            ? languageTypes.eng
            : languageTypes.ru;

    lang.innerText = languageState.toUpperCase();
});
